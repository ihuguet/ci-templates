#
# THIS FILE IS GENERATED, DO NOT EDIT
#

################################################################################
#
# Fedora checks
#
################################################################################

include:
  - local: '/templates/fedora.yml'


stages:
  - fedora_container_build
  - fedora_check

workflow:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "parent_pipeline"'
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS'
      when: never

#
# Common variable definitions
#
.ci-image-fedora:
  image: $CI_REGISTRY_IMAGE/container-build-base:2024-01-31.0

.ci-variables-fedora:
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'wget curl'
    FDO_DISTRIBUTION_EXEC: 'sh test/script.sh'
    TEST_FILE_PATH: '/test_file'
    FDO_DISTRIBUTION_VERSION: '40'
    FDO_EXPIRES_AFTER: '1h'
    FDO_CBUILD: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/cbuild/sha256-a91d5a842375f38f3901bf802be80b6d11b9937679f1dd2432b700a95ded441a/cbuild

.ci-variables-fedora@x86_64:
  extends: .ci-variables-fedora
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-x86_64-$CI_PIPELINE_ID

.ci-variables-fedora@aarch64:
  extends: .ci-variables-fedora
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID

#
# A few templates to avoid writing the image and stage in each job
#
.fedora:ci@container-build@x86_64:
  extends:
    - .fdo.container-build@fedora
    - .ci-variables-fedora@x86_64
    - .ci-image-fedora
  stage: fedora_container_build


.fedora:ci@container-build@aarch64:
  extends:
    - .fdo.container-build@fedora
    - .ci-variables-fedora@aarch64
    - .ci-image-fedora
  tags:
    - aarch64
  stage: fedora_container_build


#
# generic fedora checks
#
.fedora@check@x86_64:
  extends:
    - .ci-variables-fedora@x86_64
    - .fdo.distribution-image@fedora
  stage: fedora_check
  script:
      # run both curl and wget because one of those two is installed and one is
      # in the base image, but it depends on the distro which one
    - curl --insecure https://gitlab.freedesktop.org
    - wget --no-check-certificate https://gitlab.freedesktop.org
      # make sure our test script has been run
    - if [[ -e /test_file ]] ;
      then
        echo $FDO_DISTRIBUTION_EXEC properly run ;
      else
        exit 1 ;
      fi


#
# straight fedora build and test
#
fedora:40@container-build@x86_64:
  extends: .fedora:ci@container-build@x86_64


fedora:40@check@x86_64:
  extends: .fedora@check@x86_64
  needs:
    - fedora:40@container-build@x86_64

# Test FDO_BASE_IMAGE. We don't need to do much here, if our
# FDO_DISTRIBUTION_EXEC script can run curl+wget this means we're running on
# the desired base image. That's good enough.
fedora:40@base-image@x86_64:
  extends: fedora:40@container-build@x86_64
  stage: fedora_check
  variables:
    # We need to duplicate FDO_DISTRIBUTION_TAG here, gitlab doesn't allow nested expansion
    FDO_BASE_IMAGE: $CI_REGISTRY/$CI_PROJECT_PATH/fedora/40:fdo-ci-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_PACKAGES: ''
    FDO_DISTRIBUTION_EXEC: 'test/test-wget-curl.sh'
    FDO_FORCE_REBUILD: 1
    FDO_DISTRIBUTION_TAG: fdo-ci-baseimage-x86_64-$CI_PIPELINE_ID
  needs:
    - fedora:40@container-build@x86_64

#
# /cache fedora check (in build stage)
#
# Also ensures setting FDO_FORCE_REBUILD will do the correct job
#
fedora@cache-container-build@x86_64:
  extends: .fedora:ci@container-build@x86_64
  before_script:
      # The template normally symlinks the /cache
      # folder, but we want a fresh new one for the
      # tests.
    - mkdir runner_cache_$CI_PIPELINE_ID
    - uname -a | tee runner_cache_$CI_PIPELINE_ID/foo-$CI_PIPELINE_ID

  artifacts:
    paths:
      - runner_cache_$CI_PIPELINE_ID/*
    expire_in: 1 week

  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-cache-x86_64-$CI_PIPELINE_ID
    FDO_DISTRIBUTION_EXEC: 'bash test/test_cache.sh $CI_PIPELINE_ID'
    FDO_CACHE_DIR: $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID
    FDO_FORCE_REBUILD: 1

#
# /cache fedora check (in check stage)
#
fedora@cache-check@x86_64:
  stage: fedora_check
  image: alpine:latest
  script:
    # in the previous stage (fedora@cache-container-build@x86_64),
    # test/test_cache.sh checked for the existance of `/cache/foo-$CI_PIPELINE_ID`
    # and if it found it, it wrote `/cache/bar-$CI_PIPELINE_ID`.
    #
    # So if we have in the artifacts `bar-$CI_PIPELINE_ID`, that means
    # 2 things:
    # - /cache was properly mounted while building the container
    # - the $FDO_CACHE_DIR has been properly written from within the
    #   building container, meaning the /cache folder has been successfully
    #   updated.
    - if [ -e $CI_PROJECT_DIR/runner_cache_$CI_PIPELINE_ID/bar-$CI_PIPELINE_ID ] ;
      then
        echo Successfully read/wrote the cache folder, all good ;
      else
        echo FAILURE while retrieving the previous artifacts ;
        exit 1 ;
      fi
  needs:
    - job: fedora@cache-container-build@x86_64
      artifacts: true

#
# FDO_USER fedora check (in build stage)
#
# Also ensures setting FDO_FORCE_REBUILD will do the correct job
#
fedora@fdo-user-container-build@x86_64:
  extends: .fedora:ci@container-build@x86_64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-fdo-user-x86_64-$CI_PIPELINE_ID
    FDO_FORCE_REBUILD: 1
    FDO_USER: testuser

#
# FDO_USER fedora check (in check stage)
#
fedora@fdo-user-check@x86_64:
  extends:
    - .ci-variables-fedora@x86_64
    - .fdo.distribution-image@fedora
  stage: fedora_check
  script:
    - if [ $(whoami) != "testuser" ]; then
        echo "Wrong username $(whoami)" ;
        exit 1 ;
      fi
    - if [ $HOME != "/home/testuser" ]; then
        echo "Wrong HOME env $HOME" ;
        exit 1 ;
      fi
    # Make sure workdir and $HOME are writable by testuser
    - touch testfile
    - touch $HOME/testfile
  needs:
    - job: fedora@fdo-user-container-build@x86_64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-fdo-user-x86_64-$CI_PIPELINE_ID



fedora:40@container-build@aarch64:
  extends: .fedora:ci@container-build@aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID


fedora:40@check@aarch64:
  extends: .fedora@check@x86_64
  tags:
    - aarch64
  variables:
    FDO_DISTRIBUTION_TAG: fdo-ci-aarch64-$CI_PIPELINE_ID
  needs:
    - fedora:40@container-build@aarch64


#
# make sure we do not rebuild the image if the tag exists (during the check)
#
do not rebuild fedora:40@container-build@x86_64:
  extends: .fedora:ci@container-build@x86_64
  stage: fedora_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - fedora:40@container-build@x86_64


#
# check if the labels were correctly applied
#
check labels fedora@x86_64:40:
  extends:
    - fedora:40@check@x86_64
  image: $CI_REGISTRY_IMAGE/container-build-base:2024-01-31.0
  script:
    # FDO_DISTRIBUTION_IMAGE still has indirections
    - DISTRO_IMAGE=$(eval echo ${FDO_DISTRIBUTION_IMAGE})

    # retrieve the infos from the registry (once)
    - JSON_IMAGE=$(skopeo inspect docker://$DISTRO_IMAGE)

    # parse all the labels we care about
    - IMAGE_PIPELINE_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.pipeline_id"]')
    - IMAGE_JOB_ID=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.job_id"]')
    - IMAGE_PROJECT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.project"]')
    - IMAGE_COMMIT=$(echo $JSON_IMAGE | jq -r '.Labels["fdo.commit"]')

    # some debug information
    - echo $JSON_IMAGE
    - echo $IMAGE_PIPELINE_ID $CI_PIPELINE_ID
    - echo $IMAGE_JOB_ID
    - echo $IMAGE_PROJECT $CI_PROJECT_PATH
    - echo $IMAGE_COMMIT $CI_COMMIT_SHA

    # ensure the labels are correct (we are on the same pipeline)
    - '[[ x"$IMAGE_PIPELINE_ID" == x"$CI_PIPELINE_ID" ]]'
    - '[[ x"$IMAGE_JOB_ID" != x"" ]]' # we don't know the job ID, but it must be set
    - '[[ x"$IMAGE_PROJECT" == x"$CI_PROJECT_PATH" ]]'
    - '[[ x"$IMAGE_COMMIT" == x"$CI_COMMIT_SHA" ]]'
  needs:
    - fedora:40@container-build@x86_64


# check if b2c is working here
fedora:40@b2c@x86_64:
  extends:
    - .fdo.b2c-image@fedora
    - .ci-variables-fedora@x86_64
  stage: fedora_check
  image: $CI_REGISTRY_IMAGE/qemu-base:2024-01-31.0
  script:
    - RED='\033[0;31m'
    - GREEN='\033[0;32m'
    - ORANGE='\033[0;33m'
    - BLUE='\033[0;34m'
    - PURPLE='\033[0;35m'
    - CYAN='\033[0;36m'
    - NC='\033[0m' # No Color

    - |
      echo -e "${BLUE}******************************************
      ${BLUE}*
      ${BLUE}* ${PURPLE}Starting new test:
      ${BLUE}*
      ${BLUE}*   ${ORANGE}check if files in the current working dir are correctly synched
      ${BLUE}*
      ${BLUE}******************************************"

    - touch file_to_find.txt
    - export B2C_COMMAND="ls file_to_find.txt"
    - /app/boot2container

    - |
      echo -e "${BLUE}******************************************
      ${BLUE}*
      ${BLUE}* ${PURPLE}Starting new test:
      ${BLUE}*
      ${BLUE}*   ${ORANGE}check if we can get files from the b2c run
      ${BLUE}*
      ${BLUE}******************************************"

    - export B2C_COMMAND="touch file_from_b2c.txt"
    - /app/boot2container
    - ls file_from_b2c.txt

    - |
      echo -e "${BLUE}******************************************
      ${BLUE}*
      ${BLUE}* ${PURPLE}Starting new test:
      ${BLUE}*
      ${BLUE}*   ${ORANGE}check curl
      ${BLUE}*
      ${BLUE}******************************************"
    - export B2C_COMMAND="curl --insecure https://gitlab.freedesktop.org"
    - /app/boot2container

    - |
      echo -e "${BLUE}******************************************
      ${BLUE}*
      ${BLUE}* ${PURPLE}Starting new test:
      ${BLUE}*
      ${BLUE}*   ${ORANGE}check wget
      ${BLUE}*
      ${BLUE}******************************************"
    - export B2C_COMMAND="wget --no-check-certificate https://gitlab.freedesktop.org"
    - /app/boot2container
  needs:
   - fedora:40@container-build@x86_64


#
# make sure we do not rebuild the image if the tag exists in the upstream
# repository (during the check)
# special case where FDO_REPO_SUFFIX == ci_templates_test_upstream
#
pull upstream fedora:40@container-build@x86_64:
  extends: .fedora:ci@container-build@x86_64
  stage: fedora_check
  variables:
    FDO_UPSTREAM_REPO: $CI_PROJECT_PATH
    FDO_REPO_SUFFIX: fedora/ci_templates_test_upstream
    FDO_DISTRIBUTION_PACKAGES: 'this-package-should-not-exist'
  needs:
    - fedora:40@container-build@x86_64

#
# Try our fedora scripts with other versions and check
#

fedora:39@container-build@x86_64:
  extends: .fedora:ci@container-build@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: '39'

fedora:39@check@x86_64:
  extends: .fedora@check@x86_64
  variables:
    FDO_DISTRIBUTION_VERSION: '39'
  needs:
    - fedora:39@container-build@x86_64
